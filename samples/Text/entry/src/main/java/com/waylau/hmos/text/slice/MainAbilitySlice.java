package com.waylau.hmos.text.slice;

import com.waylau.hmos.text.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Line;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initListener();
    }

    private void initListener() {
        findComponentById(ResourceTable.Id_entry_AutoScrollingAbility).setClickedListener(this);
        findComponentById(ResourceTable.Id_entry_ColorSizeAbility).setClickedListener(this);
        findComponentById(ResourceTable.Id_entry_ItalicWeightAbility).setClickedListener(this);
        findComponentById(ResourceTable.Id_entry_AlignmentAbility).setClickedListener(this);
        findComponentById(ResourceTable.Id_entry_LinesAbility).setClickedListener(this);
        findComponentById(ResourceTable.Id_entry_AutoFontSizeAbility).setClickedListener(this);
        //二、AbilitySlice 碎片跳转
        findComponentById(ResourceTable.Id_entry_TitleDetailAbility).setClickedListener(component -> present(new TitleDetailAbilitySlice(), new Intent()));
    }

    @Override
    public void onClick(Component component) {
        //一、Ability 页面跳转
        Intent intent = new Intent();
        Intent.OperationBuilder operationBuilder = new Intent.OperationBuilder().withBundleName("com.zhaoyj.hmos.helloworld");
        switch (component.getId()) {
            case ResourceTable.Id_entry_AutoScrollingAbility:
                operationBuilder.withAbilityName(AutoScrollingAbility.class.getName());
                break;
            case ResourceTable.Id_entry_ColorSizeAbility:
                operationBuilder.withAbilityName(ColorSizeAbility.class.getName());
                break;
            case ResourceTable.Id_entry_ItalicWeightAbility:
                operationBuilder.withAbilityName(ItalicWeightAbility.class.getName());
                break;
            case ResourceTable.Id_entry_AlignmentAbility:
                operationBuilder.withAbilityName(AlignmentAbility.class.getName());
                break;
            case ResourceTable.Id_entry_LinesAbility:
                operationBuilder.withAbilityName(LinesAbility.class.getName());
                break;
            case ResourceTable.Id_entry_AutoFontSizeAbility:
                operationBuilder.withAbilityName(AutoFontSizeAbility.class.getName());
                break;
        }
        intent.setOperation(operationBuilder.build());
        startAbility(intent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
