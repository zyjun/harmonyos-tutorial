package com.waylau.hmos.text;

import com.waylau.hmos.text.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
// 参考：https://blog.csdn.net/qq_38436214/article/details/108792248
// 参考：https://blog.csdn.net/y280903468/article/details/112131586
// 参考：https://www.cnblogs.com/HarmonyOS/p/14206278.html、
// 参考：https://blog.csdn.net/y280903468/article/details/111712904
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
